#include <stdio.h>

int main(int argc, char **argv) {
  int width = 10;
  int height = 10;
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      printf("(%d,%d)", i, j);
    }
    putchar('\n');
  }
  return 0;
}
