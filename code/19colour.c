#include <math.h>
#include <stdio.h>


int channel(float c) {
  return fminf(fmaxf(roundf(255 * c), 0), 255);
}

void hsv2rgb(float h, float s, float v, float *red, float *grn, float *blu) {
  float i, f, p, q, t, r, g, b;
  if (s == 0) { r = g = b = v; } else {
    h = 6 * (h - floorf(h));
    int ii = i = floorf(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      default:r = v; g = p; b = q; break;
    }
  }
  *red = r;
  *grn = g;
  *blu = b;
}

void colour(int *r, int *g, int *b, int final_n) {
  float hue = final_n / 64.0f;
  float sat = (final_n & 1) ? 0.75f : 0.25f;
  float val = (final_n == 0) ? 0.0f :
              (final_n & 2) ? 0.75f : 0.25f;
  float red, grn, blu;
  hsv2rgb(hue, sat, val, &red, &grn, &blu);
  *r = channel(red);
  *g = channel(grn);
  *b = channel(blu);
}

int main() {
  int width = 0;
  int height = 0;
  if (2 == scanf("P6 %d %d 255", &width, &height)) {
    if (getchar() == '\n') {
      printf("P6\n%d %d\n255\n", width, height);
      for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
          int r = getchar();
          int g = getchar();
          int b = getchar();
          int final_n = (r << 16) | (g << 8) | b;
          colour(&r, &g, &b, final_n);
          putchar(r);
          putchar(g);
          putchar(b);
        }
      }
    }
  }
  return 0;
}
