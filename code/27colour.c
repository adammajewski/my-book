#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int channel(float c) {
  return fminf(fmaxf(roundf(255 * c), 0), 255);
}

void hsv2rgb(float h, float s, float v, float *red, float *grn, float *blu) {
  float i, f, p, q, t, r, g, b;
  if (s == 0) { r = g = b = v; } else {
    h = 6 * (h - floorf(h));
    int ii = i = floorf(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      default:r = v; g = p; b = q; break;
    }
  }
  *red = r;
  *grn = g;
  *blu = b;
}

void colour(int *r, int *g, int *b, int final_n, float final_z_abs, float final_z_arg) {
  float continuous_escape_time = final_n - log2f(final_z_abs + 1.0f);
  int grid =
    0.05 < final_z_abs && final_z_abs < 0.95 &&
    0.05 < final_z_arg && final_z_arg < 0.95;
  float hue = continuous_escape_time / 64.0f;
  float sat = grid * 0.7f;
  float val = 1.0f;
  if (final_n == 0) {
    sat = 0.0f;
    val = 0.0f;
  }
  float red, grn, blu;
  hsv2rgb(hue, sat, val, &red, &grn, &blu);
  *r = channel(red);
  *g = channel(grn);
  *b = channel(blu);
}






int main(int argc, char **argv) {
  char *stem = "out";
  if (argc > 1) {
    stem = argv[1];
  }
  int length = strlen(stem) + 100;

  char *fname_n = calloc(length, 1);
  snprintf(fname_n, length, "%s_n.ppm", stem);
  FILE *f_n = fopen(fname_n, "rb");
  if (! f_n) { return 1; }
  free(fname_n);
  int width_n = 0;
  int height_n = 0;
  if (2 != fscanf(f_n, "P6 %d %d 255", &width_n, &height_n)) { return 1; }
  if (fgetc(f_n) != '\n') { return 1; }

  char *fname_r = calloc(length, 1);
  snprintf(fname_r, length, "%s_z_abs.ppm", stem);
  FILE *f_r = fopen(fname_r, "rb");
  if (! f_r) { return 1; }
  free(fname_r);
  int width_r = 0;
  int height_r = 0;
  if (2 != fscanf(f_r, "P6 %d %d 255", &width_r, &height_r)) { return 1; }
  if (fgetc(f_r) != '\n') { return 1; }

  char *fname_a = calloc(length, 1);
  snprintf(fname_a, length, "%s_z_arg.ppm", stem);
  FILE *f_a = fopen(fname_a, "rb");
  if (! f_a) { return 1; }
  free(fname_a);
  int width_a = 0;
  int height_a = 0;
  if (2 != fscanf(f_a, "P6 %d %d 255", &width_a, &height_a)) { return 1; }
  if (fgetc(f_a) != '\n') { return 1; }

  if (width_n != width_r) { return 1; }
  if (width_n != width_a) { return 1; }
  if (height_n != height_r) { return 1; }
  if (height_n != height_a) { return 1; }
  int width = width_n;
  int height = height_n;

  printf("P6\n%d %d\n255\n", width, height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      int r_n = fgetc(f_n);
      int g_n = fgetc(f_n);
      int b_n = fgetc(f_n);
      int r_r = fgetc(f_r);
      int g_r = fgetc(f_r);
      int b_r = fgetc(f_r);
      int r_a = fgetc(f_a);
      int g_a = fgetc(f_a);
      int b_a = fgetc(f_a);
      int   n =  (r_n << 16) | (g_n << 8) | b_n;
      float r = ((r_r << 16) | (g_r << 8) | b_r) / (float) (1 << 24);
      float a = ((r_a << 16) | (g_a << 8) | b_a) / (float) (1 << 24);
      int red, grn, blu;
      colour(&red, &grn, &blu, n, r, a);
      putchar(red);
      putchar(grn);
      putchar(blu);
    }
  }

  fclose(f_n);
  fclose(f_a);

  return 0;
}
