#include <stdio.h>

int main(int argc, char **argv) {
  int width = 72;
  int height = 22;
  float r = 0.9;
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      float x = (i - width /2.0) / (width /2.0);
      float y = (j - height/2.0) / (height/2.0);
      if (x * x + y * y < r * r) {
        putchar('x');
      } else {
        putchar('-');
      }
    }
    putchar('\n');
  }
  return 0;
}
