#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  int width = 72;
  int height = 22;
  float aspect = 0.5;
  complex float center = 0;
  float radius = 2;
  int maximum_iterations = 256;
  float escape_radius = 2;
  if (argc > 3) {
    center = atof(argv[1]) + I * atof(argv[2]);
    radius = atof(argv[3]);
  }
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      float x = (i - width /2.0) / (height/2.0) * aspect;
      float y = (j - height/2.0) / (height/2.0);
      complex float c = center + radius * (x + I * y);
      complex float z = 0;
      for (int n = 0; n < maximum_iterations; ++n) {
        z = z * z + c;
      }
      if (cabs(z) <= escape_radius) {
        putchar('x');
      } else {
        putchar('-');
      }
    }
    putchar('\n');
  }
  return 0;
}
