
/* 

gcc -std=c99 -Wall -Wextra -pedantic -O3 18.c -lm

./a.out

 


created from 18render.c and 18.colour.c 
with :

gcc render.c  -E -o 18render.c

*/


#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <stdio.h>

int channel(float c) {
  return fminf(fmaxf(roundf(255 * c), 0), 255);
}

void hsv2rgb(float h, float s, float v, float *red, float *grn, float *blu) {
  float i, f, p, q, t, r, g, b;
  if (s == 0) { r = g = b = v; } else {
    h = 6 * (h - floorf(h));
    int ii = i = floorf(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      default:r = v; g = p; b = q; break;
    }
  }
  *red = r;
  *grn = g;
  *blu = b;
}

void colour2(int *r, int *g, int *b, int final_n) {
  float hue = final_n / 64.0f;
  float sat = (final_n & 1) ? 0.75f : 0.25f;
  float val = (final_n == 0) ? 0.0f :
              (final_n & 2) ? 0.75f : 0.25f;
  float red, grn, blu;
  hsv2rgb(hue, sat, val, &red, &grn, &blu);
  *r = channel(red);
  *g = channel(grn);
  *b = channel(blu);
}

void colour(int *r, int *g, int *b, int final_n) {
  *r = final_n >> 16;
  *g = final_n >> 8;
  *b = final_n;
}




float cabs2f(_Complex float z) {
  return crealf(z) * crealf(z) + cimagf(z) * cimagf(z);
}

_Complex float coordinatef(int i, int j, int width, int height, _Complex float center, float radius) {
  float x = (i - width /2.0f) / (height/2.0f);
  float y = (j - height/2.0f) / (height/2.0f);
  _Complex float c = center + radius * (x - (__extension__ 1.0iF) * y);
  return c;
}

int calculatef(_Complex float c, int maximum_iterations, float escape_radius2) {
  _Complex float z = 0;
  int final_n = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2f(z) > escape_radius2) {
      final_n = n;
      break;
    }
  }
  return final_n;
}

void renderf(int width, int height, _Complex float center, float radius, int maximum_iterations, float escape_radius2) {
  printf("P6\n%d %d\n255\n", width, height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      _Complex float c = coordinatef(i, j, width, height, center, radius);
      int final_n = calculatef(c, maximum_iterations, escape_radius2);
      int r, g, b;
      colour(&r, &g, &b, final_n);
      // manual change
      final_n = (r << 16) | (g << 8) | b;
      colour2(&r, &g, &b, final_n);
      //
      putchar(r);
      putchar(g);
      putchar(b);
    }
  }
}







double cabs2(_Complex double z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

_Complex double coordinate(int i, int j, int width, int height, _Complex double center, double radius) {
  double x = (i - width /2.0) / (height/2.0);
  double y = (j - height/2.0) / (height/2.0);
  _Complex double c = center + radius * (x - (__extension__ 1.0iF) * y);
  return c;
}

int calculate(_Complex double c, int maximum_iterations, double escape_radius2) {
  _Complex double z = 0;
  int final_n = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2(z) > escape_radius2) {
      final_n = n;
      break;
    }
  }
  return final_n;
}

void render(int width, int height, _Complex double center, double radius, int maximum_iterations, double escape_radius2) {
  printf("P6\n%d %d\n255\n", width, height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      _Complex double c = coordinate(i, j, width, height, center, radius);
      int final_n = calculate(c, maximum_iterations, escape_radius2);
      int r, g, b;
      colour(&r, &g, &b, final_n);
      // manual change
      final_n = (r << 16) | (g << 8) | b;
      colour2(&r, &g, &b, final_n);
      //
      putchar(r);
      putchar(g);
      putchar(b);
    }
  }
}







long double cabs2l(_Complex long double z) {
  return creall(z) * creall(z) + cimagl(z) * cimagl(z);
}

_Complex long double coordinatel(int i, int j, int width, int height, _Complex long double center, long double radius) {
  long double x = (i - width /2.0l) / (height/2.0l);
  long double y = (j - height/2.0l) / (height/2.0l);
  _Complex long double c = center + radius * (x - (__extension__ 1.0iF) * y);
  return c;
}

int calculatel(_Complex long double c, int maximum_iterations, long double escape_radius2) {
  _Complex long double z = 0;
  int final_n = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2l(z) > escape_radius2) {
      final_n = n;
      break;
    }
  }
  return final_n;
}

void renderl(int width, int height, _Complex long double center, long double radius, int maximum_iterations, long double escape_radius2) {
  printf("P6\n%d %d\n255\n", width, height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      _Complex long double c = coordinatel(i, j, width, height, center, radius);
      int final_n = calculatel(c, maximum_iterations, escape_radius2);
      int r, g, b;
      colour(&r, &g, &b, final_n);
      // manual change
      final_n = (r << 16) | (g << 8) | b;
      colour2(&r, &g, &b, final_n);
      //
      putchar(r);
      putchar(g);
      putchar(b);
    }
  }
}
 

int main(int argc, char **argv) {
  int width = 1280;
  int height = 720;
  _Complex long double center = 0;
  long double radius = 2;
  int maximum_iterations = 4096;
  long double escape_radius = 2;
  long double escape_radius2 = escape_radius * escape_radius;
  int float_type = 1;
  if (argc > 3) {
    center = strtold(argv[1], 0) + (__extension__ 1.0iF) * strtold(argv[2], 0);
    radius = strtold(argv[3], 0);
  }
  if (argc > 4) {
    float_type = atoi(argv[4]);
  }
  switch (float_type) {
    case 0:
      printf("use float type \n");
      renderf(width, height, center, radius, maximum_iterations, escape_radius2);
      break;
    case 1:
      printf("use double type \n");
      render (width, height, center, radius, maximum_iterations, escape_radius2);
      break;
    case 2:
      printf("use long double type \n");
      renderl(width, height, center, radius, maximum_iterations, escape_radius2);
      break;
  }
  return 0;
}
