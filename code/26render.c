#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel {
  int final_n;
  _Complex float final_z;
};

struct image {
  int width;
  int height;
  struct pixel *pixels;
};




float pif = 3.14159265358979323846264338327950288419716939937510f;

float cabs2f(_Complex float z) {
  return crealf(z) * crealf(z) + cimagf(z) * cimagf(z);
}

_Complex float coordinatef(int i, int j, int width, int height, _Complex float center, float radius) {
  float x = (i - width /2.0f) / (height/2.0f);
  float y = (j - height/2.0f) / (height/2.0f);
  _Complex float c = center + radius * (x - (__extension__ 1.0iF) * y);
  return c;
}

void calculatef(struct pixel *out, _Complex float c, int maximum_iterations, float escape_radius2) {
  out->final_n = 0;
  out->final_z = 0;
  _Complex float z = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2f(z) > escape_radius2) {
      out->final_n = n;
      out->final_z = z;
      break;
    }
  }
}

void renderf(struct image *img, _Complex float center, float radius, int maximum_iterations, float escape_radius2) {
#pragma omp parallel for
  for (int j = 0; j < img->height; ++j) {
    for (int i = 0; i < img->width; ++i) {
      _Complex float c = coordinatef(i, j, img->width, img->height, center, radius);
      calculatef(&img->pixels[j * img->width + i], c, maximum_iterations, escape_radius2);
    }
  }
}

double pi = 3.14159265358979323846264338327950288419716939937510;

double cabs2(_Complex double z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

_Complex double coordinate(int i, int j, int width, int height, _Complex double center, double radius) {
  double x = (i - width /2.0) / (height/2.0);
  double y = (j - height/2.0) / (height/2.0);
  _Complex double c = center + radius * (x - (__extension__ 1.0iF) * y);
  return c;
}

void calculate(struct pixel *out, _Complex double c, int maximum_iterations, double escape_radius2) {
  out->final_n = 0;
  out->final_z = 0;
  _Complex double z = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2(z) > escape_radius2) {
      out->final_n = n;
      out->final_z = z;
      break;
    }
  }
}

void render(struct image *img, _Complex double center, double radius, int maximum_iterations, double escape_radius2) {
#pragma omp parallel for
  for (int j = 0; j < img->height; ++j) {
    for (int i = 0; i < img->width; ++i) {
      _Complex double c = coordinate(i, j, img->width, img->height, center, radius);
      calculate(&img->pixels[j * img->width + i], c, maximum_iterations, escape_radius2);
    }
  }
}

long double pil = 3.14159265358979323846264338327950288419716939937510l;

long double cabs2l(_Complex long double z) {
  return creall(z) * creall(z) + cimagl(z) * cimagl(z);
}

_Complex long double coordinatel(int i, int j, int width, int height, _Complex long double center, long double radius) {
  long double x = (i - width /2.0l) / (height/2.0l);
  long double y = (j - height/2.0l) / (height/2.0l);
  _Complex long double c = center + radius * (x - (__extension__ 1.0iF) * y);
  return c;
}

void calculatel(struct pixel *out, _Complex long double c, int maximum_iterations, long double escape_radius2) {
  out->final_n = 0;
  out->final_z = 0;
  _Complex long double z = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2l(z) > escape_radius2) {
      out->final_n = n;
      out->final_z = z;
      break;
    }
  }
}

void renderl(struct image *img, _Complex long double center, long double radius, int maximum_iterations, long double escape_radius2) {
#pragma omp parallel for
  for (int j = 0; j < img->height; ++j) {
    for (int i = 0; i < img->width; ++i) {
      _Complex long double c = coordinatel(i, j, img->width, img->height, center, radius);
      calculatel(&img->pixels[j * img->width + i], c, maximum_iterations, escape_radius2);
    }
  }
}




struct image *image_new(int width, int height) {
  struct image *img = calloc(1, sizeof(*img));
  if (! img) { return 0; }
  img->width = width;
  img->height = height;
  img->pixels = calloc(width * height, sizeof(*img->pixels));
  if (! img->pixels) { free(img); return 0; }
  return img;
}

void image_free(struct image *img) {
  free(img->pixels);
  free(img);
}

int image_save(struct image *img, char *filestem, float escape_radius) {
  int retval = 0;

  int length = strlen(filestem) + 100;
  char *filename = calloc(length, 1);
  if (filename) {

    snprintf(filename, length, "%s_n.ppm", filestem);
    FILE *f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          int n = img->pixels[j * img->width + i].final_n;
          fputc(n >> 16, f);
          fputc(n >> 8, f);
          fputc(n , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    snprintf(filename, length, "%s_z_abs.ppm", filestem);
    f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          _Complex float z = img->pixels[j * img->width + i].final_z;
          float r = logf(cabsf(z)) / logf(escape_radius) - 1.0f;
          int n = (1 << 24) * r;
          fputc(n >> 16, f);
          fputc(n >> 8, f);
          fputc(n , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    snprintf(filename, length, "%s_z_arg.ppm", filestem);
    f = fopen(filename, "wb");
    if (f) {
      fprintf(f, "P6\n%d %d\n255\n", img->width, img->height);
      for (int j = 0; j < img->height; ++j) {
        for (int i = 0; i < img->width; ++i) {
          _Complex float z = img->pixels[j * img->width + i].final_z;
          float t = fmodf(cargf(z) / (2.0f * pif) + 1.0f, 1.0f);
          int n = (1 << 24) * t;
          fputc(n >> 16, f);
          fputc(n >> 8, f);
          fputc(n , f);
        }
      }
      fclose(f);
    } else {
      retval = 1;
    }

    free(filename);
  } else {
    retval = 1;
  }

  return retval;
}


int main(int argc, char **argv) {
  int retval = 1;
  int width = 1280;
  int height = 720;
  _Complex long double center = 0;
  long double radius = 2;
  int maximum_iterations = 4096;
  long double escape_radius = 2;
  if (argc > 3) {
    center = strtold(argv[1], 0) + (__extension__ 1.0iF) * strtold(argv[2], 0);
    radius = strtold(argv[3], 0);
  }
  if (argc > 4) {
    escape_radius = strtold(argv[4], 0);
  }
  long double escape_radius2 = escape_radius * escape_radius;
  char *stem = "out";
  if (argc > 5) {
    stem = argv[5];
  }
  int float_type = 1;
  long double pixel_spacing = radius / (height / 2.0);
  int pixel_spacing_bits = -log2l(pixel_spacing);
  if (pixel_spacing_bits > 50) {
    float_type = 2;
  }
  if (pixel_spacing_bits > 60) {
    fprintf(stderr, "WARNING insufficient precision (need %d bits)\n", pixel_spacing_bits);
  }

  struct image *img = image_new(width, height);
  if (img) {
    switch (float_type) {
      case 0:
        fprintf(stderr, "INFO using float\n");
        renderf(img, center, radius, maximum_iterations, escape_radius2);
        break;
      case 1:
        fprintf(stderr, "INFO using double\n");
        render (img, center, radius, maximum_iterations, escape_radius2);
        break;
      case 2:
        fprintf(stderr, "INFO using long double\n");
        renderl(img, center, radius, maximum_iterations, escape_radius2);
        break;
    }
    int err = image_save(img, stem, escape_radius);
    image_free(img);
    if (! err) {
      retval = 0;
    } else {
      fprintf(stderr, "ERROR image save failed\n");
    }
  } else {
    fprintf(stderr, "ERROR image new failed\n");
  }
  return retval;
}
