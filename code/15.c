#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

float cabs2f(complex float z) {
  return crealf(z) * crealf(z) + cimagf(z) * cimagf(z);
}

double cabs2(complex double z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

complex float coordinatef(int i, int j, int width, int height, complex float center, float radius) {
  float x = (i - width /2.0f) / (height/2.0f);
  float y = (j - height/2.0f) / (height/2.0f);
  complex float c = center + radius * (x - I * y);
  return c;
}

complex double coordinate(int i, int j, int width, int height, complex double center, double radius) {
  double x = (i - width /2.0) / (height/2.0);
  double y = (j - height/2.0) / (height/2.0);
  complex double c = center + radius * (x - I * y);
  return c;
}

int calculatef(complex float c, int maximum_iterations, float escape_radius2) {
  complex float z = 0;
  int final_n = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2f(z) > escape_radius2) {
      final_n = n;
      break;
    }
  }
  return final_n;
}

int calculate(complex double c, int maximum_iterations, double escape_radius2) {
  complex double z = 0;
  int final_n = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    z = z * z + c;
    if (cabs2(z) > escape_radius2) {
      final_n = n;
      break;
    }
  }
  return final_n;
}

void renderf(int width, int height, complex float center, float radius, int maximum_iterations, float escape_radius2) {
  printf("P5\n%d %d\n255\n", width, height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      complex float c = coordinatef(i, j, width, height, center, radius);
      int final_n = calculatef(c, maximum_iterations, escape_radius2);
      putchar(final_n);
    }
  }
}

void render(int width, int height, complex double center, double radius, int maximum_iterations, double escape_radius2) {
  printf("P5\n%d %d\n255\n", width, height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      complex double c = coordinate(i, j, width, height, center, radius);
      int final_n = calculate(c, maximum_iterations, escape_radius2);
      putchar(final_n);
    }
  }
}

int main(int argc, char **argv) {
  int width = 1280;
  int height = 720;
  complex double center = 0;
  double radius = 2;
  int maximum_iterations = 256;
  double escape_radius = 2;
  double escape_radius2 = escape_radius * escape_radius;
  int use_double = 0;
  if (argc > 3) {
    center = atof(argv[1]) + I * atof(argv[2]);
    radius = atof(argv[3]);
  }
  if (argc > 4) {
    use_double = atoi(argv[4]);
  }
  if (use_double) {
    render (width, height, center, radius, maximum_iterations, escape_radius2);
  } else {
    renderf(width, height, center, radius, maximum_iterations, escape_radius2);
  }
  return 0;
}
