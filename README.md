# Description
This is a clone of project by Claude Heiland-Allen
*  http://mathr.co.uk/blog/2014-03-06_mandelbrot_notebook.html
*  http://code.mathr.co.uk/mandelbrot-book

How to write a book about the Mandelbrot set.

See also: 
* [wikibooks](https://en.wikibooks.org/wiki/Fractals/book)
* [fork of pdf file](https://gitlab.com/adammajewski/git-book-pdf)
* [single file c programs ](https://gitlab.com/adammajewski/mandelbrot-book_book)
* external ray ( exray)
  * [m_d_exray_in](https://gitlab.com/adammajewski/m_d_exray_in)
  * [NonInteractiveParameterRayInMPFR](https://gitlab.com/adammajewski/NonInteractiveParameterRayInMPFR)
* nucleus 
  * [Mandelbrot_set_centers_list](https://gitlab.com/adammajewski/Mandelbrot_set_centers_list_by_Claude)
  * [mandelbrot-nucleus-c-double](https://gitlab.com/adammajewski/mandelbrot-nucleus-c-double)
  * [mandelbrot-numerics-nucleus](https://gitlab.com/adammajewski/mandelbrot-numerics-nucleus)
  * [mandelbrot-nucleus-mpc](https://gitlab.com/adammajewski/mandelbrot-nucleus-mps)
  * [mandelbrot-nucleus-mpfr](https://gitlab.com/adammajewski/mandelbrot-nucleus-mpfr)
* [mandelbrot-numerics-box-periood ](https://gitlab.com/adammajewski/mandelbrot-numerics-box-periood)
* [wake_gmp](https://gitlab.com/adammajewski/wake_gmp)
* zoom
  * [zoom_precision](https://gitlab.com/adammajewski/zoom_precision)




 
 The difference from original is that Claude uses diffs only 
 and I use full c code in the form of 1 file programs,
 

# Files
* pdf ( use make from console to create it from tex file  )
* tex file
* png images (created from pnm or png images  )
* c source files 
* bash source code ( sh files)

 
# Programming techniques 
* Decoupling rendering from image output
* Parallel rendering with OpenMP
* Automatic number type selection 
* using c, preprocessor and bash 
* writing to the standard output stream and redirecting it to a file with the shell
* graphic file formats called PNM, the “portable anymap file format”
 

## 1.26 Automatic number type selection

We’re telling our render program which number type to use. We can improve this by calculating
the best number type to use, using the spacing between pixels. We compute the number of bits
of precision needed to distinguish points with a magnitude around 1 using a base-2 logarithm.
We default to double precision because it’s the fastest, but if we need more precision we upgrade
to long double. If long double still insuffices, we print a warning message on the standard error
stream. We also print information messages so we know which number type our program is
using.

```bash
for exp in $(seq -w 1 30)
do
./render -1.26031888962837738806438608314184 0.38347214027113837724841765113596 2e-${exp} ${exp}\
&& ./colour ${exp} > ${exp}out.ppm
done
```

The result :

```bash
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using double
INFO using long double
INFO using long double
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
(need 63 bits)
(need 67 bits)
(need 70 bits)
(need 73 bits)
(need 77 bits)
(need 80 bits)
(need 83 bits)
(need 87 bits)82
CHAPTER 1. THE EXTERIOR
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
WARNING insufficient precision
INFO using long double
```


![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/00_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/01_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/02_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/03_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/04_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/05_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/06_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/07_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/08_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/09_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/10_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/11_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/12_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/13_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/14_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/15_ppm.png "")


![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/16_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/17_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/18_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/19_ppm.png "")

![](/images/bd7aa445bfe2b02a2883dccdb561c58dcc620aeb/20_ppm.png "")


The last 4 images are indeed increasingly pixelated.


 
# Fractal algorithms 


![]( "")


## Exterior

### 1.15 Escape time colouring


### 1.23 Final angle colouring


Our colour function uses both sources information now: 
* hue coming from the escape time
* value from the final angle

Angles of complex number are unique up to multiples of a whole turn (2 pi), so maths conventionalized the prinicipal argument to be in [-pi,pi]. Given a fixed range, we can
rescale it to the fixed range for our PPM output: first we normalize to [-0.5,0.5], we shift it by 1 to [0.5,1.5], and then we use the fmod() function to take the fractional part, giving us a range
of [0,1]. Multiplying by the number of values in our 24bit range gives us an integer that we can pack into bytes of colour as before.






n

![](/images/99e73f934ddd79ebdbbb2f6d929e6ffd3562df9a/00_n_ppm.png "n")

z_arg

![](/images/99e73f934ddd79ebdbbb2f6d929e6ffd3562df9a/00_z_arg_ppm.png "z_arg")


![](/images/99e73f934ddd79ebdbbb2f6d929e6ffd3562df9a/00_ppm.png "")

### 1.27 Final radius colouring

We’ve got the final z iterate, but we’re only using the phase because we weren’t sure how to
scale the magnitude. We know the magnitude is greater than the escape radius: 

∣zn ∣ > R. 

We also know that the previous iterate’s magnitude must have been less than R, otherwise we would
have broken out of the loop earlier: 


∣z(n−1) ∣ <= R

Combining with the definition of the iteration,

zn = z(n−1)* z(n-1) + c, 

and using the triangle inequality 


∣a + b∣ <= ∣a∣ + ∣b∣, 

we get:

R < ∣zn ∣ <= ∣z(n−1)|^ 2 + ∣c∣ <= R^2 + ∣c∣

If we make R bigger, so that ∣c∣ fades into insignificance, we get the approximation 

R < ∣zn ∣ <= R^2 

 and taking base-R logarithms we get
 
1 < log∣zn ∣/logR <= 2

Now we can subtract 1 to get a value in [0,1], which we know how to pack into a 24bit PPM
file. We can use this for colouring, and we can vary the escape radius to see how big it needs to
be for the approximation to give acceptable results:

```bash
for er in 2 4 8 16 32 64 128 256 512 1024
do
./render 0 0 4 ${er} ${er} &&
./colour ${er} > ${er}.ppm
done
```

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/2_ppm.png "2")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/4_ppm.png "4")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/8_ppm.png "8")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/16_ppm.png "16")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/32_ppm.png "32")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/64_ppm.png "64")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/128_ppm.png "128")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/256_ppm.png "256")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/512_ppm.png "512")

![](/images/bbd262f1df0b02d41c829800db414930c3f94e54/1024_ppm.png "1024")

Looking at the output images, it’s clear that the approximation behaves badly for small R,
with glitches at the edges of the bands. We also see that the shapes change, with the cells formed
by the angle colouring becoming longer and thinner. With the escape radius at 512, the cells
look almost square, and there are no visible gltiches.



### 1.28 Smooth colouring with continuous escape time

The escape time bands around the Mandelbrot set increase stepwise by 1. We now have the final
iterate radius, which we can use to smooth the colouring. If the final radius is large, then the
previous iterate was close to escaping. If the final radius is small, then the previous iterate was
further from escaping. Here we assume the escape radius is large enough for the approximation
ignoring |c| to hold. Squaring gives a curve, which we can linearize using logarithms. We have a
value in [0,1], but log 0 is undefined (it heads towards negative infinity), moreover we still want
to end up with a value in [0,1]. Adding 1 gives a value in [1,2], and taking the base-2 logarithm
gives the required range, as 

log_B(1) = 0 

and 

log_B(B) = 1 

for all B. We subtract this value from the integer escape time, giving a fractional escape time with continuity at the transitions between
bands and smoothly increasing size.

```bash
for exp in $(seq -w 0 11)
do
./render 0.3060959246356990742873 0.0237427672737983365906 \
2e-${exp} 512 ${exp} && ./colour ${exp} > ${exp}.ppm
done
```
![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/00_ppm.png "0")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/01_ppm.png "1")


![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/02_ppm.png "2")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/03_ppm.png "3")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/04_ppm.png "4")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/05_ppm.png "5")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/06_ppm.png "6")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/07_ppm.png "7")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/08_ppm.png "8")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/09_ppm.png "9")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/10_ppm.png "10")

![](/images/fb98ed31d9265937092682c1f04d0c34aecff54f/11_ppm.png "11")


 
### 1.29 generating a grid 

Grid is showing :
* how the escape time bands get closer together towards the boundary of the Mandelbrot set
*  also how the angle cells double-up within each successive band.

![1.29 generating a grid :  showing how the escape time bands get closer together towards the boundary of the Mandelbrot
set, and also how the angle cells double-up within each successive band. ](/images/00d0fa5735a6b5f2ddeb0d8453a7403594a7baa0/00_ppm.png "plain exterior grid")

### 1.30 Improving the grid 

The grid has noticeable step changes in the width of the rays heading towards the Mandelbrot
set, because each successive escape time band has double the number of cells, so at the boundary
between bands there is a discontinuity where the width halves. We can compensate for this by
making the rays wider (in local coordinate terms) further out and thinner further in. The inside
end should be twice as narrow as the outside end, to match up with the halving of the width
when crossing band boundaries. We keep the middle the same width as before by ensuring the
width factor is 1 there, which means the power should be 0 at the fractional radius 0.5. Now the
rays of the grid seem to continue along a smooth path towards the Mandelbrot set.


![](/images/6255e2b5e432f65e1318d0704b9dbe3b1b0041a8/00_ppm.png "")


### 1.31 Distance estimation (for removing aliasing near the boundary of Mandelbrot set)


Our images look noisy and grainy near the boundary of the Mandelbrot set. The escape time
bands get closer and closer, while the pixel spacing is fixed. The pixel grid samples isolated
points of a mathematically abstract image defined on the continuous plane. The Nyquist-Shannon
sampling theorem shows that sampling isolated points from a continuum is a valid approximation
only so long as the values don’t change too quickly between the points. Aliasing occurs when
the values do change too quickly compared to the sampling rate, with the grainy noisy visual
effects as we have seen. Because the escape time bands increase in number without bound as we
approach the boundary of the Mandelbrot set, no sampling rate can be high enough.

....


The distance estimate has the useful property (proved in the Koebe 1/4 theorem) that it tells
us roughly how far our point is from the boundary of the Mandelbrot set. We can use it to mask
the grainy noise from aliasing, with the pleasing side-effect of highlighting the intricate shape of
the boundary of the Mandelbrot set with all its filaments and shrunken copies. To contrast with
the dark boundary, we fill the interior with white.


```bash
for exp in $(seq -w 0 16)
do
./render \
-1.2864956446267794623797546990264761111 \
0.4339437613619272464268001250115015311 \
2e-${exp} 512 ${exp} &&
./colour ${exp} > ${exp}.ppm
done
```
![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/00_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/01_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/02_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/03_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/04_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/05_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/06_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/07_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/08_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/09_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/10_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/11_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/12_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/13_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/14_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/15_ppm.png "")

![](/images/e7ff7b7158c999b8fcc24e627c39226233181e38/16_ppm.png "")




### 1.32 Removing distracting colour

The bold colours are now a distraction: they don’t show us anything we can’t see from the grid or the distance estimation. Desaturating the image and making the exterior grid a light grey
gives us a clean style that is easy on the eyes.

![](/images/52a33c0fe498cf918bd6c6ce8324c2697fb44546/00_ppm.png "improved gray grid")

### 1.33 Refactoring image stream reading.
Our colour.c program has a lot of repetition. We factor it out into smaller functions, with a
struct stream to hold the necessary data for each input image. We ensure that stream_new()
returns a non-null pointer only for valid images. Reading the channel bytes and packing them
into an int is performed in stream_read(). We write a wrapper to form an appropriate filename
from a stem and a suffix. Now the main() is a lot shorter, with more robust error handling. We
proceed to the image processing loop only if all the input files were opened and are compatible,
and signal a successful return value. We close all the streams that were opened successfully
before exit.


### 1.34 Annotating images with Cairo.

We’d like to add labels and other annotations to our Mandelbrot set images. Cairo is a library
for vector graphics, with support for rasterized image surfaces. We factor out the stream reading
code to a separate file, and use it in both our ./colour program and our new ./annotate program
which so far just reads the coloured image and saves it to compressed PNG (Portable Network
Graphics) format.

### 2.2 Atom domains

Points in the exterior eventually escape to infinity. This means that the minimum iterate magni-
tude is well defined, and we can use the iteration count to define the atom domain as the iteration
count when this minimum is reached. Colouring according to atom domain reveals shadows of
the blobs in the previous section - we can read off the period of the blob with red being 1, orange
2, yellow-green 3, and so on. We can also see that the period 3 bulbs have antenna hubs with 3
spokes, and period 4 bulbs have hubs with 4 spokes


![](/images/fdc8ed916d0158b5a67ccffaa124a18c4be20226/out_ppm.png "")





### external ray


#### Newton's Method: Ray In

The:
* next point r along an external ray with current doubled angle $`\theta`$
* current depth p 
* current radius R 

satisfies:

$`F^p(0,r)=\lambda R e^{2 \pi i \theta}`$


where $`\lambda < 1`$ controls the sharpness of the ray.  


Applying Newton's method in one complex variable gives:  

$`r_{m+1} = r_m - \frac{F^p(0,r_m) - \lambda R e^{2 \pi i \theta}}{\frac{\partial}{\partial c}F^p(0,r_m)}`$


When crossing dwell bands:
* double $`\theta`$
* increment p,
* resetting the radius R.  

Stop tracing when close to the target (for example when within the basin of attraction for Newton's method for nucleus.

#### Newton's Method: Ray Out

>>>
you need to trace a ray outwards, which means using different C values, and the bits come in reverse order, first the deepest bit from the
iteration count of the start pixel, then move C outwards along the ray (perhaps using the newton's method of mandel-exray.pdf in reverse),
repeat until no more bits left.  you move C a fractional iteration count 
each time, and collect bits when crossing integer dwell boundaries
>>>




When crossing dwell bands, compute the doubling preimages of $`\theta`$:  
* $`\theta_-`$
* $`\theta_+`$

using equations:  
$`\theta_- = \frac{\theta}{2}`$  
$`\theta_+ = \frac{\theta + 1}{2}`$  

Then compute:
* $`r_{-}`$ 
* $`r_{+}`$   
using Newton's method.  

Choose the nearest to $`r_m`$ to be $`r_{m+1}`$.  

Collect a list of which choices were made to determine the final $`\theta`$ in high precision without relying on the bits of $`\theta`$ itself.  

$`\lambda > 1`$ controls the sharpness of the ray.  

Stop tracing when r reaches the escape radius.


## Interior

2.6 Interior coordinates ( page 135)

![2.6 Interior coordinates ( page 135) ](/images/4c1484033a7b20ac109cd91ca1165b86cdf7513e/out_ppm.png "2.6 Interior coordinates ( page 135)")




# License

This project is licensed under the  Creative Commons Attribution-ShareAlike 4.0 International License - see the [LICENSE.md](LICENSE.md) file for details

# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)




## Git
```
git init
git remote add origin git@gitlab.com:adammajewski/my-book.git
git add html
git commit -m "html"
git push -u origin master
```


