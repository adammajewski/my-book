book.pdf: book.tex
	pdflatex book.tex
	pdflatex book.tex
	pdflatex book.tex

web: book.pdf
	gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -sOutputFile=book-draft-`date --iso`-web.pdf book.pdf

.PHONY: web
